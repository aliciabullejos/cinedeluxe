
import Programa.Cliente;
import baseDatos.conexion;
import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.*;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Alicia-Portatil
 */
public class Registrar extends JFrame{
   
    JTextField nombre, apellido1, apellido2, email, usuario, contrasena; 
    JButton registrar, volver; 
    JLabel Lnombre, Lapellido1, Lapellido2, Lemail, Lusuario, Lcontrasena; 
    conexion bd=new conexion();
  Cliente cl=new Cliente();
    
  public Registrar(){
    /*Para montar entera el JFrame*/   
    IniciarComponentes();
   /* Titulo de la ventana*/
     setTitle("Registrate");
   /*Tamaño*/   
      setSize(500,600);
   /*inmovible y centrada*/    
    setResizable(false);
    setLocationRelativeTo(null);      
  }  
    
  public void IniciarComponentes(){
      
      //Montamos el contenedos
  
       setLayout(null);
  
    this.Lusuario=new JLabel("Usuario: "); 
    Lusuario.setBounds(50,50 ,180, 33);
    Lusuario.setForeground(Color.MAGENTA.darker()); //cambio de color un label
    Lusuario.setFont(new Font("Serif", Font.BOLD, 20)); //ponerle funte y tamaño
    this.usuario=new JTextField("",15);  
    usuario.setBounds(200, 50, 200, 30);
  
    this.Lcontrasena=new JLabel("Contraseña: "); 
    Lcontrasena.setBounds(50,130 ,180, 33);
    Lcontrasena.setForeground(Color.MAGENTA.darker()); //cambio de color un label
    Lcontrasena.setFont(new Font("Serif", Font.BOLD, 20)); //ponerle funte y tamaño
    this.contrasena=new JTextField("",15);  
    contrasena.setBounds(200, 130, 200, 30);
  
    this.Lnombre=new JLabel("Nombre: "); 
   Lnombre.setBounds(50,210 ,200, 33);
   Lnombre.setForeground(Color.MAGENTA.darker()); //cambio de color un label
    Lnombre.setFont(new Font("Serif", Font.BOLD, 18)); //ponerle funte y tamaño
    this.nombre=new JTextField("",15);
    nombre.setBounds(200, 210, 200, 30);
    
    this.Lapellido1=new JLabel("Apellido primero: "); 
    Lapellido1.setBounds(50,290 ,180, 33);
    Lapellido1.setForeground(Color.MAGENTA.darker()); //cambio de color un label
    Lapellido1.setFont(new Font("Serif", Font.BOLD, 18)); //ponerle funte y tamaño
    this.apellido1=new JTextField("",15);
     apellido1.setBounds(200, 290, 200, 30);
    
  this.Lapellido2=new JLabel("Apellido segundo: "); 
  Lapellido2.setBounds(50,370 ,180, 33);
  Lapellido2.setForeground(Color.MAGENTA.darker()); //cambio de color un label
  Lapellido2.setFont(new Font("Serif", Font.BOLD, 18)); //ponerle funte y tamaño
  this.apellido2=new JTextField("",15);
  apellido2.setBounds(200, 370, 200, 30);
  
   this.Lemail=new JLabel("Email: "); 
  Lemail.setBounds(50, 450 ,180, 33);
  Lemail.setForeground(Color.MAGENTA.darker()); //cambio de color un label
  Lemail.setFont(new Font("Serif", Font.BOLD, 18)); //ponerle funte y tamaño
  this.email=new JTextField("",15);
  email.setBounds(200, 450, 200, 30);
  
  this.registrar=new JButton("Registrar"); 
  registrar.setBounds(200,500, 100,30 );
  registrar.addActionListener(new OyenteRegistrar());
  
  this.volver=new JButton("Volver"); 
  volver.setBounds(350,500, 100,30 );
  volver.addActionListener(new OyenteVolver());


  add(Lusuario);  add(usuario);
 add(Lcontrasena); add(contrasena);
  add(Lnombre); add(nombre);
  add(Lapellido1); add(apellido1);
  add(Lapellido2);  add(apellido2);
  add(Lemail); add(email);
  add(registrar); add(volver); 
 

  }
  
/**
 * Obtengo todos los valores que necesito para insertar en la base de datos. 
 * Hago una condicion para validar los datos y que cumplan las ordenes qu ele pido, esa condicion llama al 
 * metodo donde se gestiona y si se cumple ya si que se podra realizar la insercion llamando claro a otro metodo insertar
 * en el cual se le pasan los valores por parametro. 
 */
  class OyenteRegistrar implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent ae) {
          
            String vacio=" ";
            
           String u=usuario.getText();
           String p=contrasena.getText();
           String n=nombre.getText();
           String a1=apellido1.getText();
           String a2=apellido2.getText();
           String e=email.getText();
       if((u.equals(""))||(p.equals(""))||(p.equals(""))||(n.equals(""))||(a1.equals(""))||(a2.equals(""))||(e.equals(""))){
                 JOptionPane.showMessageDialog( null, "no pueden existir elementos vacíos");
       }else{
           if(cl.validarDatos(u)){   
                     cl.registrarUsuario(u,p,n,a1,a2,e);
                     login_cliente logCli=new login_cliente();
                     logCli.setVisible(true);
                     Registrar.this.dispose();
                }   
                else{
                    JOptionPane.showMessageDialog( null, "El usuario debe tener entre 5 y 10 caracteres, "
                            + "el primer valor debe ser una letra");


                }
       }
          
        }      
  }
  
 class OyenteVolver implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent ae) {
         
            login_cliente lg=new login_cliente();
            lg.setVisible(true);
            dispose();
            
           
       }
          
        }      
  }
  
  

