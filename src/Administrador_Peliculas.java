
 
import Programa.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Vector;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
 
/**
 *
 * @author Alicia-Portatil
 */
public class Administrador_Peliculas extends JFrame {
  JMenuBar op;
  JMenuItem salir, volver; //lista desplegable;
  JTabbedPane tabPanel;
  JPanel crear, borrar, mostrar;
  JTextField ide,titulo,director,duracion,precio, hora, imagen, video, sipnosis;
  JTextField ide2,titulo2,director2,duracion2,precio2, hora2;
  JButton elimino,  agrega, muestra;
  JTextField jtfNum1, jtfNum2, jtfResult;
  DefaultTableModel dtm;
  JTable tabla; 
    JScrollPane panel ;
Administrador ad=new Administrador();
  
  
  public Administrador_Peliculas(){
     
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE );
        setLayout( new BorderLayout() );
        setSize( 900, 500);
        setTitle( "Administracion Peliculas" );
        setLocationRelativeTo(null);
         //ventana estatica
        setResizable(false); //ventana fija
     
       
        menu1();  // ACTIVA EL MENU 1
 
        panelCrear();
        panelBorrar();
        panelMostrar();
         
        // Montar las lenguetas en la ventana
        tabPanel = new JTabbedPane();
        /* aqui se crean las pestañas a partir de los paneles */
        tabPanel.addTab( "Añadir nueva Pelicula" , crear);
        tabPanel.addTab( "Borrar Pelicula" , borrar );
        tabPanel.addTab( "Mostrar" , mostrar);
       
     
        /**
         * Se pone lo ultimo para arregar todo el tabPanel con sus cosas agragadas al contenedor de la clase
         */
       add( tabPanel, BorderLayout.CENTER);
     
        setVisible(true);
 
  }
 
  public void menu1()
  {
    // COMPONENTES DEL MENU  
      
    // Barra de menú
    op=new JMenuBar();
     setJMenuBar(op); // añadimos la barra  de menu al panel
 
    //cremos otra objeto
    JMenu Menu = new JMenu("Opciones");
    op.add(Menu);
     Menu.add(salir = new JMenuItem("Cerrar", 'C'));
     Menu.add(volver = new JMenuItem("Volver", 'V'));
     
       //manejadore de salir
      salir.addActionListener(new OyenteMenu());
      volver.addActionListener(new OyenteMenu1());
  }
 
 
 
  public void panelCrear()
  {  
        // PANEL DE LA LENGUETA
      //creamos panel para que crear pueda contener cosas
        crear = new JPanel();
        crear.setLayout(null); //como se van a organizar los componentes
 
        // COMPONENTES DEL PANEL- añadimos los componentes  
       //Introducimos los datos con JTextField
       JLabel id=new JLabel("Identificacion");
       crear.add(id);
       crear.add(ide = new JTextField("",15)); //el 5 es el tamaño
       id.setBounds(50,20,180,25);
       ide.setBounds(150,20,150, 30);
 
        JLabel titu=new JLabel("Titulo");
        crear.add(titu);
        crear.add(titulo= new JTextField("",15));
        titu.setBounds(50,60,180,25);
        titulo.setBounds(150,60,300, 30);
       
        JLabel di=new JLabel("Director");
        crear.add(di);
        crear.add(director = new JTextField("",15));
        di.setBounds(50,100,180,25);
        director.setBounds(150,100,300, 30);
         
        JLabel du= new JLabel("Duracion");
        crear.add(du);
        crear.add(duracion= new JTextField("",15));
        du.setBounds(50,140,180,25);
        duracion.setBounds(150,140,100, 30);
         
       JLabel pre =new JLabel("Precio");
        crear.add(pre);
        crear.add(precio= new JTextField("",15));
        pre.setBounds(50,180,180,25);
        precio.setBounds(150,180,100, 30);
         
        JLabel ho =new JLabel("Horario");
        crear.add(ho);
        crear.add(hora= new JTextField("",15));
        ho.setBounds(50,220,180,25);
        hora.setBounds(150,220,100, 30);
       
        JLabel sip =new JLabel("Sipnosis");
        crear.add(sip);
        crear.add(sipnosis=new JTextField());
        sipnosis.setBorder(BorderFactory.createLineBorder(Color.blue, 1));
        sip.setBounds(610,20,180,25);
        sipnosis.setBounds(490, 50, 300, 300);
        
        JLabel foto=new JLabel("Imagen");
        crear.add(foto);
        crear.add(imagen=new  JTextField("",15));
        foto.setBounds(50,260, 180,25);
        imagen.setBounds(150, 260, 300, 30);
 
        JLabel vide=new JLabel("Video");
        crear.add(vide);
        crear.add(video=new  JTextField("",15));
        vide.setBounds(50,300, 180,25);
        video.setBounds(150, 300, 300, 30);
        
        
              //logon empresa
       JLabel logo=new JLabel();
       logo.setIcon(new ImageIcon(getClass().getResource("/imagenes/logoC.png")));
       crear.add(logo);
       logo.setBounds(50, 300, 100, 100);
       
        agrega= new JButton(); //crear boton
        agrega.setText("Agregar");//darle  nombre
        agrega.setBounds(250,350, 100, 35);
        crear.add(agrega); //añadir
        agrega.addActionListener(new OyenteAgregar());
         
         //fondo
       JLabel imagen=new JLabel();
        imagen.setIcon(new ImageIcon(getClass().getResource("/imagenes/F2.png")));
       crear.add(imagen);
       imagen.setBounds(0, 0, 900, 600);
   
     
      
         
  }
 
  
   
    public void panelBorrar()
  {
   
      borrar = new JPanel();
      borrar.setLayout(null); //como se van a organizar los componentes
       
       JLabel id=new JLabel("Identificacion");
       borrar.add(id);
       borrar.add(ide2 = new JTextField("",15)); //el 5 es el tamaño
       id.setBounds(50,20,180,25);
       ide2.setBounds(150,20,150, 30);
       

       
       elimino= new JButton(); //crear boton
       elimino.setText("Borrar");//darle  nombre
       elimino.setBounds(250,350, 100, 35);
       borrar.add(elimino); //borrar
       elimino.addActionListener(new OyenteBorrar());
       
       muestra=new JButton("Ver peliculas en mostrar");
       muestra.setBounds(400,350,200, 35);
       borrar.add(muestra);
       muestra.addActionListener(new OyenteMostrar());
     
            //logon empresa
       JLabel logo=new JLabel();
       logo.setIcon(new ImageIcon(getClass().getResource("/imagenes/logoC.png")));
       borrar.add(logo);
       logo.setBounds(50, 300, 100, 100);
       
           //fondo
       JLabel imagen=new JLabel();
        imagen.setIcon(new ImageIcon(getClass().getResource("/imagenes/F2.png")));
       borrar.add(imagen);
       imagen.setBounds(0, 0, 900, 600);

 
  }
 
 
     public void panelMostrar()
  {
        mostrar = new JPanel();
        mostrar.setPreferredSize( new Dimension(100,50) );
        mostrar.setLayout( new BorderLayout() );
       
     Vector columnas = new Vector();
       
        /* 
            A ese vector le agrego datos, estos datos vendrán a ser las
            columnas de la tabla.
        */
        columnas.add("Id");
        columnas.add("Titulo");
        columnas.add("Director");
        columnas.add("Duracion");
         columnas.add("precio");
        columnas.add("horario");
        columnas.add("sipnosis");
        columnas.add("imagenes");
         columnas.add("video");
        
        /* 
        La clase AbstractTableModel es la que implementa directamente a la interface
        TableModel, aunque es esta clase la que se recomienda extender para utilizarla como modelo de
        tabla, existe un modelo de tabla predeterminado que facilita mucho el trabajo con tablas. Este
        modelo predeterminado es la clase DefaultTableModel
        */
        
        dtm= new DefaultTableModel(null, columnas);
        
         
        /* 
            Despues de haber creado el modelo de tabla, dtm en el ejemplo , se crea la tabla con el
            constructor correspondiente:
         */
        tabla = new JTable(dtm);
        /** Creo una instancia de JScrollPane y le paso como parametro la tabla.
        *Un JScrollPane proporciona una vista desplazable de un componente ligero.
        *Cuando el estado de la pantalla real está limitado, se utiliza un ScrollPane para mostrar un componente
        *que es grande o cuyo tamaño puede cambiar dinámicamente. 
        **/
         panel =new JScrollPane(tabla);
        /* Por ultimo agrego ese objeto de JScrollPane (barra desplazamiento) al contenedor de la ventana */
        mostrar.add(panel);     
        
  }
    
 
 class OyenteMenu implements ActionListener {
        public void actionPerformed(ActionEvent e)
        {
           
                          
            int selector=JOptionPane.showOptionDialog(salir, "¿Quiere salir?", "Salir", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE,null,null,null); 
            
            if(selector==0){

                  System.exit(0);
            }
                       
                   
             }
 }

 class OyenteMenu1 implements ActionListener {
        public void actionPerformed(ActionEvent e)
        {

                    PanelAdministrador p=new PanelAdministrador();   
                    p.setVisible(true);
                    dispose();
                   
             }
 }
               
   class OyenteAgregar implements ActionListener {
        public void actionPerformed(ActionEvent e)
        {
            boolean valor=false;
           String ident = ide.getText();  
          String title=titulo.getText();
           String d=director.getText();
           String dur = duracion.getText();
           String pre=precio.getText();
            if (pre.matches("[0-9]*")){
                  int pre1 = Integer.parseInt(pre);
                  String h=hora.getText();
                  String im=imagen.getText();
                  String vi=video.getText();
                  String s=sipnosis.getText(); 
           
                                    
                                if (ad.validarDatos(ident, pre1)) {               
                                        valor=ad.insertarPelicula(ident,title,d,dur,pre1,h,s,im,vi);
                                        } else{
                                                    JOptionPane.showMessageDialog( null, "Identificador debe tener como maximo cinco caracteres y comenzar con un valor numerio.  "
                                                        + "Precio debe ser un valor positivo mayor a cero" );
                                                  }
                                  }else{
                                                     JOptionPane.showMessageDialog( null, "Precio debe ser un valor numerico" );
                                  }
                       
                         
     
           if(valor==true){
             JOptionPane.showMessageDialog( null, "Pelicula Insertada" );  
                        ide.setText("");
                       titulo.setText("");
                       director.setText("");
                       duracion.setText("");
                        precio.setText("");
                       hora.setText("");
                       imagen.setText("");
                       video.setText("");
                      sipnosis.setText(""); 
             
           }else{
               JOptionPane.showMessageDialog( null, "Error no se puede insertar");
            
           }
           
        } 
       
      
   }
 

   class OyenteBorrar implements ActionListener {
        public void actionPerformed(ActionEvent e)
        {
           String ident = ide2.getText();           
          boolean valor=ad.borrarPelicula(ident);
       
           if(valor==true){
             JOptionPane.showMessageDialog( null, "Pelicula borrada" );  
                        ide2.setText("");
                 
           }else{
               JOptionPane.showMessageDialog( null, "Error al introducir id" );
            
           }
           
        }
 }
    /**
   * A este evento le estamos insertando a un tabla con SetModel el resultado del Metodo MostrarCines que es 
   * una tabla tambien. 
   */
   class OyenteMostrar implements ActionListener {
        public void actionPerformed(ActionEvent e)
        {
            
         tabla.setModel(ad.MostrarPelicula());

   }
     
}          
}         
