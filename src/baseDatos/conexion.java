package baseDatos;

import Programa.Cines;
import Programa.Cliente;
import Programa.Pelicula;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

public class conexion {
    //declaro una conexion
    Connection conexion=null;
    //constructor
    
    public conexion(){
        
        try {
            
            /**
             * UTILIZO ESTO EN VEZ DE USAR LA CLASE PROPERTIES.
             */
            Class.forName("org.gjt.mm.mysql.Driver");
            String usuario="root";
            String password=""; //no tiene contraseña
            String base="cinedeluxe"; //nombre de la base de datos
            
            
            //incializo la conexion - 3306 es mi puerto de mysql/ local
            conexion=DriverManager.getConnection("jdbc:mysql://localhost:3306/cinedeluxe",usuario,password);
            System.out.println("Se realizo la conexion satisfactoriamente");//para saber si conecta con bd
         
            
         } catch (ClassNotFoundException ex) {
            System.out.println("Error en el driver"+ex.getMessage());
         }catch(SQLException e){ //capturo excepcion de sql 
             System.out.println("Error en la conexion"+e.getStackTrace());
         } 
        
    }
    //metodo que nos regresa lo que contiene la base de datos
    //nos regresa una variable de este tipo defaulttable model
    

/**
 * Comprobar si el usuario existe y que la contraseña pertenece a ese usuario en concreto
 * @param usuario
 * @param contrasena
 * @return 
 */
      public boolean comprobarUsuario(String usuario, String contrasena){
          boolean validar = false;
        try{
            Statement stmt = conexion.createStatement(); 
            String consulta = "SELECT * FROM cliente WHERE usuario ='" +usuario  + "' AND password = '"+contrasena+"'";
           
            ResultSet resultadoConsulta = stmt.executeQuery(consulta);
        /**
         * resultadoConsulta.next sirve para recorrer mediante un bucle todo lo que tiene la consulta.
         */
            while (resultadoConsulta.next()) {
           /**con esta condicion realizo que si el login que le paso por parametros y la contraseña que le he pasado por 
          *parametros son iguales a alguna de las que tengo en mi base de datos, y corresponde uno a otro
          **/
                if(usuario.equals(resultadoConsulta.getString("usuario")) && contrasena.equals(resultadoConsulta.getString("password"))){
            /**nos devuelve true, si a encontrado el login y el pass igual dentro de la base de datos, 
               * sino no devolvera false (que esta por defecto), y eso significaria que no se encuentra en la base de datos
              */
                         validar = true;  //Abre la sesion
                        System.out.println("Usuario Correcto"); 

                            /**
                             * Introducir los datos del usuario en el programa
                             */
                                     String usu=resultadoConsulta.getString("usuario");
                                     String pass=resultadoConsulta.getString("password");
                                     String no=resultadoConsulta.getString("nombre");
                                     String ap1=resultadoConsulta.getString("apellido1");
                                     String ap2=resultadoConsulta.getString("apellido2");
                                     String em=resultadoConsulta.getString("email");
                                                                  
                
                }else{
                    validar=false;
                    System.out.println("Error, el usuario es incorrecto");
                   
                }
            }
        }
        catch(Exception e){
          System.out.println("Error al iniciar sesion" +e.getMessage());
        }
        //devuelvo un true o un false, cuestion de si se cumple mi condicion de arriba o no
        return validar;
    }
      
      /**
       * Comprobar que el usuario existe y que la contraseña es la de ese usuario en concreto
       * @param usuario
       * @param contrasena
       * @return 
       */
       public boolean comprobarAdministrador(String usuario, String contrasena){
          boolean validar = false;
        try{
            Statement stmt = conexion.createStatement(); 
            String consulta = "SELECT * FROM administrador WHERE usuario ='" +usuario  + "' AND password = '"+contrasena+"'";
           
            ResultSet resultadoConsulta = stmt.executeQuery(consulta);
        /**
         * resultadoConsulta.next sirve para recorrer mediante un bucle todo lo que tiene la consulta.
         */
            while (resultadoConsulta.next()) {
           /**con esta condicion realizo que si el login que le paso por parametros y la contraseña que le he pasado por 
          *parametros son iguales a alguna de las que tengo en mi base de datos, y corresponde uno a otro
          **/
                if(usuario.equals(resultadoConsulta.getString("usuario")) && contrasena.equals(resultadoConsulta.getString("password"))){
            /**nos devuelve true, si a encontrado el login y el pass igual dentro de la base de datos, 
               * sino no devolvera false (que esta por defecto), y eso significaria que no se encuentra en la base de datos
              */
                        System.out.println("datos correctos");
                         validar = true;  //Abre la sesion
                         
                }else{
                    System.out.println("Error, el usuario es incorrecto");
                    validar=false;
                    
                }
            }
        }
        catch(Exception e){
          System.out.println("Error al iniciar sesion" +e.getMessage());
        }
        //devuelvo un true o un false, cuestion de si se cumple mi condicion de arriba o no
        return validar;
    }
 

      /**
       * Mostar en el combobox una lista con las diferentes peliculas de la bd, pero tan solo 
       * muerta los titulos
       * @return lista ArrayList
       */
    
        public  ArrayList MostrarComboBox(){
           ArrayList <String> p=new ArrayList<>();
        
        //ahora realizo una consulta a base de datos
        //declaramos una consulta a la base de datos
        PreparedStatement consulta;
        try {
            //ahora recojo los datos
            consulta=conexion.prepareStatement("SELECT titulo FROM peliculas");
            ResultSet rs=consulta.executeQuery();
            while(rs.next()){
                String peliculas= rs.getString("titulo");
                p.add(peliculas);
            }
        } catch (SQLException ex) {
            System.out.println("Error en la consulta"+ex.getMessage());
        }
        return p;
       
       }  
        
        /**
         * Igual que en el anterior metodo pero aqui mostramos una lista de cines
         * @return Lista Arraylist
         */
      public  ArrayList MostrarComboBox1(){
           ArrayList <String> c=new ArrayList<>();
        
        //ahora realizo una consulta a base de datos
        //declaramos una consulta a la base de datos
        PreparedStatement consulta;
        try {
            //ahora recojo los datos
            consulta=conexion.prepareStatement("SELECT nombre FROM cines");
            ResultSet rs=consulta.executeQuery();
            while(rs.next()){
                String nombre=rs.getString("nombre");
                c.add(nombre);
            }
        } catch (SQLException ex) {
            System.out.println("Error en la consulta " +ex.getMessage());
        }
        return c;
       
       }  
      
      /**
       * Saca todos los valores de la pelicula cuyo parametro que introducimos sea igual al titulo
       * esos valores se los manda a un objeto de clase pelicula y asi poder mostrarlo en otra funcion.  
       * @param peli
       * @return objeto pelicula
       */
        
   public Pelicula verInfo(String peli){
            Pelicula p=new Pelicula();
        
                 try{
            Statement stmt = conexion.createStatement(); 
            String consulta = "SELECT * FROM peliculas WHERE titulo='" +peli+"'";
           
            ResultSet resultadoConsulta = stmt.executeQuery(consulta);
            while (resultadoConsulta.next()) {
                
                
                p.setTitulo(resultadoConsulta.getString("titulo"));
                p.setDirector(resultadoConsulta.getString("director"));
                p.setDuracion(resultadoConsulta.getString("duracion"));
                p.setPrecio(resultadoConsulta.getString("precio"));
                p.setHorario(resultadoConsulta.getString("horario"));
                p.setImagen(resultadoConsulta.getString("imagen"));
                p.setSipnosis(resultadoConsulta.getString("sipnosis"));
                
            }
        }
        catch(Exception e){
          System.out.println("Error en la consulta" +e.getMessage());
        }
                 return p;
    }
   
   
   /**
    * Saca los valores de los cines cuyo nombre sea igual al parametro pasado y los 
    * carga en un objeto de la clase
    * @param cine
    * @return objeto de cines
    */
      public Cines CargarCines(String cine){
            Cines c=new Cines();
        
                 try{
            Statement stmt = conexion.createStatement(); 
            String consulta = "SELECT * FROM cines WHERE nombre='" +cine+"'";
           
            ResultSet resultadoConsulta = stmt.executeQuery(consulta);
            while (resultadoConsulta.next()) {
                
                
                c.setNombre(resultadoConsulta.getString("nombre"));
                c.setCiudad(resultadoConsulta.getString("ciudad"));
            
          
            }
        }
        catch(Exception e){
          System.out.println("Error en la consulta" +e.getMessage());
        }
                 return c;
    }
      
      /**
       *  Saca los valores de los clientes cuyo nombre sea igual al parametro pasado y los 
       *  carga en un objeto de la clase
       * @param usuario
       * @return 
       */
       public Cliente CargarClientes(String usuario){
            Cliente c=new Cliente();
             
                 try{
            Statement stmt = conexion.createStatement(); 
            String consulta = "SELECT * FROM cliente WHERE usuario='" +usuario+"'";
            
                 
                ResultSet resultadoConsulta = stmt.executeQuery(consulta);
                while (resultadoConsulta.next()) {
               
                c.setLogin(resultadoConsulta.getString("usuario"));
                c.setPassword(resultadoConsulta.getString("password"));
                c.setNombre(resultadoConsulta.getString("nombre"));
                c.setApellido1(resultadoConsulta.getString("apellido1"));
                c.setApellido2(resultadoConsulta.getString("apellido2"));
                c.setMail(resultadoConsulta.getString("email"));
               
                    System.out.println(c.getNombre());
                 
                
             }
        }
        catch(Exception e){
          System.out.println("Error en la consulta" +e.getMessage());
        }
                 return c;
                 
 
    }
      /**
       * Creo una tabla en la base de datos llamada temporal en la cual se le pasaran todos los datos 
       * del cliente logeado y asi los podre usar con posterioridad en otras ventanas sin perderlos
       * @param u
       * @param p
       * @param n
       * @param a1
       * @param a2
       * @param e 
       */
       public void insertarTemporal(String u, String p, String n, String a1, String a2, String e ){

        try {
            Statement consulta1=conexion.createStatement(); //sirve para crear la consulta y haga conexion con la bd
            consulta1.executeUpdate("INSERT INTO temporal VALUES('"+u+"','"+p+"','"+n+"','"+a1+"','"+a2+"','"+e+"')");
           
        } catch (SQLException ex) {
            System.out.println("Error en la inserccion " + ex.getMessage());
            
        }        
    }
     
       /**
        * Para que el uso de la nueva tabla temporal sea correcto solo tendra una fila, la del usuario y
        * por tanto cuando se entra con un nuevo usuario la tabla se borra, es decir, se limpia por dentro 
        * dejandola vacia. 
        */
        public void borrarTemporal(){

          try {
            Statement consulta =conexion.createStatement();
            consulta.executeUpdate("TRUNCATE temporal");
        } catch (SQLException ex) {
            System.out.println("Error en el borrado");
            
        }
 
       }       
        
     
    }
      
      
      
      
       
    