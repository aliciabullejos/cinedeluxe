
import Programa.Cines;
import Programa.Cliente;
import Programa.Pelicula;
import baseDatos.conexion;
import com.toedter.calendar.JDateChooser;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import javax.swing.*;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Alicia-Portatil
 */
public class UsuarioCine extends JFrame {
   
  CardLayout cardLayout;
  Pelicula ClasePelicula=new Pelicula();
  Cines ClaseCine=new Cines();
  Cliente ClaseCliente=new Cliente();
  JMenuBar op;
   JPanel ventana, peliculas,comprar_entrada, datos;
  JMenuItem salir, Cerrar_Sesion, mis_Datos, VolverApeliculas; //componentes del menu
  JButton info, mas , menos, comprar, v, imprimir;
  JTextField numero, titulo, cine, precio;
   JTextField usuario, pass, no, ap1,ap2,mail;
  JLabel Lpelicula,Lcines, imagen, p;
  JComboBox cb,cb1;  
  JCheckBox normal, especial;
  JDateChooser calendario;

  conexion bd=new conexion();
  Cliente cl=new Cliente();
  
  public UsuarioCine(){
      
  /**permite iniciar las propiedades de los componentes*/
  iniciarComponentes();
  /**añadir ventana **/
  add(ventana);
  /**Asigna un titulo a la barra de titulo*/
  setTitle("CineDeluxe");
  /**tamaño de la ventana*/
  setSize(800,600);
  /**pone la ventana en el Centro de la pantalla*/
  setLocationRelativeTo(null);
  //ventana estatica
   setResizable(false);       
   
   
}

    private void iniciarComponentes() {
              
      menu1();
      VerDatos();
      PanelPelicula();
      PanelComprar();
      construyePanel();
      
       setVisible(true);
      
    }
    
      public void construyePanel(){
        ventana= new JPanel();
        cardLayout=new CardLayout();
        ventana.setLayout(cardLayout);
        
        /*Al agregar necesitamos 2 argumentos, el objeto a agregar y un nombre referencial */
        ventana.add(peliculas, "peliculas");
        ventana.add(comprar_entrada, "Comprar");
        ventana.add(datos, "Datos");
       
    }

    
    public void  menu1(){
          // Barra de menú
    op=new JMenuBar();
     setJMenuBar(op); // añadimos la barra  de menu al panel

    //cremos otra objeto 
    JMenu opcion = new JMenu("Opciones");
    op.add(opcion);
    opcion.add(salir = new JMenuItem("Salir", 'S'));
    opcion.add(Cerrar_Sesion=new JMenuItem("Cerrar sesion",'C'));
    opcion.add(VolverApeliculas= new JMenuItem ( "Volver a peliculas", 'V'));
    opcion.add(mis_Datos=new JMenuItem("Mis Datos",'D'));
    
   salir.addActionListener(new OyenteSalir());
   Cerrar_Sesion.addActionListener(new OyenteCerrar());
   mis_Datos.addActionListener(new OyenteDatos());
   VolverApeliculas.addActionListener(new OyenteVolverP());
   
    }
    /**
     * Abrir una ventana desde las opciones para ver todos los datos del usuario.
     */
    public void VerDatos(){
        
       datos=new JPanel();
       datos.setLayout(null);
       datos.setBackground(Color.white);

        JLabel Texto=new JLabel("DATOS    USUARIO:");
        datos.add(Texto);
      Texto.setBounds(100,50 , 320, 80);
      Texto.setForeground(Color.magenta.darker()); //cambio de color un label
      Texto.setFont(new Font("Serif", Font.BOLD, 28)); //ponerle funte y tamaño
       
       
       JLabel  Lusuario=new JLabel("Usuario:");
       Lusuario.setBounds(150, 200, 120, 33);
       usuario=new JTextField();
       usuario.setBounds(280, 200, 250,33);
       usuario.setEditable(false);
      
       JLabel  Lpass=new JLabel("Contraseña:");
       Lpass.setBounds(150, 250, 120, 33);
        pass=new JTextField();
        pass.setBounds(280, 250, 250, 33);
       pass.setEditable(false);
       
       JLabel  Lnombre=new JLabel("Nombre:");
       Lnombre.setBounds(150, 300, 120, 33);
        no=new JTextField();
        no.setBounds(280,300, 250, 33);
       no.setEditable(false);
       
       JLabel  Lap1=new JLabel("Apellido primero:");
       Lap1.setBounds(150, 350, 120, 33);
       ap1=new JTextField();
       ap1.setBounds(280, 350, 250, 33);
       ap1.setEditable(false);
       
       JLabel  Lap2=new JLabel("Apellido segundo:");
       Lap2.setBounds(150, 400, 120, 33);
         ap2=new JTextField();
         ap2.setBounds(280, 400, 250, 33);
       ap2.setEditable(false);
       
       JLabel  Lmail=new JLabel("Email:");
       Lmail.setBounds(150, 450, 120, 33);
        mail=new JTextField();
        mail.setBounds(280, 450, 250, 33);
       mail.setEditable(false);
        
       datos.add(Lusuario);datos.add(usuario);
       datos.add(Lpass);datos.add(pass);
       datos.add(Lnombre);datos.add(no);
       datos.add(Lap1);datos.add(ap1);
       datos.add(Lap2);datos.add(ap2);
       datos.add(Lmail);datos.add(mail);
    
       //Imagen fondo
  JLabel imagen=new JLabel(); 
   imagen.setIcon(new ImageIcon(getClass().getResource("/imagenes/login.png"))); 
   datos.add(imagen);
   imagen.setBounds(280, 40, 800, 600);
   
       
       
    }
    
    public void PanelPelicula(){
        
        
       peliculas=new JPanel();
       peliculas.setLayout(null);
       peliculas.setBackground(Color.white);
       
       
       Lpelicula=new JLabel("Lista de peliculas");
       peliculas.add(Lpelicula);
       Lpelicula.setBounds(50, 200, 180,33);
       Lpelicula.setFont((new Font("Arial", Font.BOLD, 20)));
         
       Lcines=new JLabel("Lista de cines");
       peliculas.add(Lcines);
       Lcines.setBounds(490, 200, 180,33);
       Lcines.setFont((new Font("Arial", Font.BOLD, 20)));
       
       info=new JButton("Ver Info");
       peliculas.add(info);
       info.setBounds(120,400, 120, 40);
       info.addActionListener(new OyenteCombo1());
       
       JLabel logoinfo=new JLabel();
       logoinfo.setIcon(new ImageIcon(getClass().getResource("/imagenes/info.png"))); 
       peliculas.add(logoinfo);
       logoinfo.setBounds(70, 400, 32, 32);
       
       /**
        * ComboBox. Inizializarlo y hacer bucle para ir mostrando cada uno de sus Item
        */
       ArrayList <String> p1=new ArrayList<>();
       p1=bd.MostrarComboBox();
       cb=new JComboBox();
       for(int x=0; x<p1.size(); x++){
           cb.addItem(p1.get(x));
       }
      
       peliculas.add(cb);
       cb.setBounds(50, 250, 240, 40);

       /**
        * Segundo comboBox .
        */
       
       ArrayList <String> c=new ArrayList<>();
       c=bd.MostrarComboBox1();
       cb1=new JComboBox();
       for(int x=0; x<c.size(); x++){
         cb1.addItem(c.get(x));
       }
       peliculas.add(cb1);
       cb1.setBounds(450, 250, 240, 40);

  //logon empresa
   JLabel logo=new JLabel(); 
   logo.setIcon(new ImageIcon(getClass().getResource("/imagenes/logo300.png"))); 
   peliculas.add(logo);
   logo.setBounds(300, 10, 200, 200);
   
    comprar=new JButton("Comprar");
    peliculas.add(comprar);
    comprar.setBounds(550,450, 100, 45);
    comprar.addActionListener(new OyenteComprar());
   
    }
    
    
    public void PanelComprar(){
      
       comprar_entrada=new JPanel();
       comprar_entrada.setLayout(null);
       comprar_entrada.setBackground(Color.white);
       
       
       Lpelicula=new JLabel("Pelicula:");
       comprar_entrada.add(Lpelicula);
       Lpelicula.setBounds(50, 50, 120,33);
       Lpelicula.setFont((new Font("Arial", Font.BOLD, 20)));
       this.titulo=new JTextField("",15);
       titulo.setBounds(150, 50, 250, 30);
       titulo.setEditable(false);
      comprar_entrada.add(titulo);
      
       Lcines=new JLabel("Cine:");
       comprar_entrada.add(Lcines);
       Lcines.setBounds(50, 100, 120,33);
       Lcines.setFont((new Font("Arial", Font.BOLD, 20))); 
        this.cine=new JTextField("",15);
        cine.setBounds(150, 100, 250, 30);
        cine.setEditable(false);
        comprar_entrada.add(cine);

      
       
        /**
        * Para determinar el numero de entradas. 
        */
       
       JLabel numeroEntradas=new JLabel("Numero de entradas");
       numeroEntradas.setFont((new Font("Arial", Font.BOLD, 14)));
       comprar_entrada.add(numeroEntradas);
       numeroEntradas.setBounds(50, 180, 180, 100);
       
       mas=new JButton("+");
       comprar_entrada.add(mas);
       mas.setBounds(50, 250, 45, 45);
       mas.addActionListener(new OyenteSumar());
       
       menos=new JButton("-");
       comprar_entrada.add(menos);
       menos.setBounds(150,250,45, 45);
       menos.addActionListener(new OyenteRestar());
       
       numero=new JTextField("0",3);
       comprar_entrada.add(numero);
       numero.setBounds(100, 250, 45, 45);
       
     
       /**
        * Para determinar el tipo de entrada.
        * Si es especial o si es normal. Mediante 
        */
       normal=new JCheckBox("Entrada normal");
       normal.setFont((new Font("Arial", Font.BOLD, 14)));
      comprar_entrada.add(normal);
       normal.setBounds(100, 300, 250, 80);
       normal.setBackground(Color.white);
       normal.addItemListener(new manejado());
    
        especial=new JCheckBox("Entrada especial");
        especial.setFont((new Font("Arial", Font.BOLD, 14)));
       comprar_entrada.add(especial);
       especial.setBounds(100, 350, 200, 80);
       especial.setBackground(Color.white);
       especial.addItemListener(new manejado());
       JLabel exp=new JLabel("* Entrada especial para menores de 11 años y para minusvalidos");
       comprar_entrada.add(exp);
       exp.setBounds(50, 500, 500,60);
      
      //Para introducirlo en un grupo y asi solo puede elegir una opcion 
       ButtonGroup grupo=new ButtonGroup();
       grupo.add(normal);
       grupo.add(especial);
       
       p=new JLabel("Precio entradas:");
       p.setBounds(50, 440, 120,33);
       p.setFont((new Font("Arial", Font.BOLD, 14)));
       comprar_entrada.add(p);
        this.precio=new JTextField("",15);
        precio.setBounds(200, 450, 80, 30);
        precio.setEditable(false);
        comprar_entrada.add(precio);
 
    v=new JButton("Volver a peliculas");
    comprar_entrada.add(v);
    v.setBounds(550,450, 150, 45);
    v.addActionListener(new OyenteVolver2());
        
    imprimir=new JButton("Imprimir entrada");
    comprar_entrada.add(imprimir);
    imprimir.setBounds(550,350, 150, 45);
    imprimir.addActionListener(new OyenteImprimir());
    
    
    //logon empresa
   JLabel logo=new JLabel(); 
   logo.setIcon(new ImageIcon(getClass().getResource("/imagenes/logo300.png"))); 
   comprar_entrada.add(logo);
   logo.setBounds(500, 10, 200, 200);
   
  calendario = new com.toedter.calendar.JDateChooser();
  comprar_entrada.add(calendario);
  calendario.setBounds(300, 220, 180, 33);
 
    
    }

class OyenteVolverP implements ActionListener {

        public void actionPerformed(ActionEvent e)
        {
            UsuarioCine u=new UsuarioCine();
            dispose(); 
        }
    }
    


    class OyenteSalir implements ActionListener {
        public void actionPerformed(ActionEvent e)
        {
   
            
            int selector=JOptionPane.showOptionDialog(salir, "¿Quiere salir?", "Salir", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE,null,null,null); 
            
            if(selector==0){

                  System.exit(0);
            }
            
        
 }}

    
     class OyenteCerrar implements ActionListener {
        public void actionPerformed(ActionEvent e)
        {
   
            login_cliente lc=new login_cliente();
            lc.setVisible(true);
            dispose();
           
        }
 }
     
        
  class OyenteVolver2 implements ActionListener {
        public void actionPerformed(ActionEvent e)
        {
         
         peliculas.setVisible(true);
         comprar_entrada.setVisible(false);
            
        }
 }
     /**
      * Los datos del usuario los carga directamente del programa.
      */
   class OyenteDatos implements ActionListener {
        public void actionPerformed(ActionEvent e)
        {
            
                 peliculas.setVisible(false);
                comprar_entrada.setVisible(false);
              datos.setVisible(true);
              ClaseCliente=cl.MostrarUsuarioTemporal();
  
            usuario.setText(ClaseCliente.getLogin());
            pass.setText(ClaseCliente.getPassword());
            no.setText(ClaseCliente.getNombre());
            ap1.setText(ClaseCliente.getApellido1());
            ap2.setText(ClaseCliente.getApellido2());
            mail.setText(ClaseCliente.getMail());
           
        }
 }

     class OyenteCombo1 implements ActionListener {
        public void actionPerformed(ActionEvent e)
        {
     
 
            /**
             * Coger la informacion del combobox
             */
            String peli=(String)cb.getSelectedItem();
            
            /**
             * De esta forma estamos pasando por el metodo verInfo de la consulta a bd todos los datos de 
             * pelicula al objeto de la clase pelicula, es decir ahora en pelicula se guarda la peli pasada por paramentros
             * que es la elegida en el combo
             */
            ClasePelicula=bd.verInfo(peli);
            
            
            /**
             * Quiero llenar la siguiente ventana con los datos de la pelicula, pero ya no consulto la bd sino que los 
             * cojo directamente del programa
             */
            InfoPelicula ip=new InfoPelicula();
            ip.setVisible(true);
            ip.titulo.setText(ClasePelicula.getTitulo());
            ip.director.setText(ClasePelicula.getDirector());
            ip.duracion.setText(ClasePelicula.getDuracion());
            ip.precio.setText(ClasePelicula.getPrecio());
            ip.horario.setText(ClasePelicula.getHorario());
            ip.sp.setText(ClasePelicula.getSipnosis());
   
              //Para poder introducir la imagen 
            ip.imagen.setIcon(new ImageIcon(getClass().getResource(ClasePelicula.getImagen())));
            
            dispose();
          
        }
 }  
     
 class OyenteComprar implements ActionListener {
        public void actionPerformed(ActionEvent e)
        {
        
         /**
         * 1- Sacar el valor del los combobox
         * 2-Del valor que salga, buscar en la base de datos y pasar la informacion al programa, la
         * funcion funcion que hace eso esta en la clase conexcion , en este caso se llama verInfo y cargarCines
         * 3- Dar los valores a los JtextField de la interfaz llamandolos desde el programa.
         */
        String peli=(String)cb.getSelectedItem();   
        String c=(String)cb1.getSelectedItem();
        
        /**Aprovecho la funcion verInfo porque al fin y al cabo hago aqui lo mismo que para lo que fue creada 
         * aunque tan solo ahora tome el valor de l titulo nada mas.
         */
        ClasePelicula=bd.verInfo(peli);
        ClaseCine=bd.CargarCines(c);
        
        /**Hace invisible una ventana, hago visible la otra y paso los datos desde el programa a los JtextFields
         * Que ya de por si fueron creados inmodificables.
         */
        
               peliculas.setVisible(false);
              comprar_entrada.setVisible(true);
        cine.setText(ClaseCine.getNombre());
        titulo.setText(ClasePelicula.getTitulo());
        
         
        }
 }
 
      //sumar
    class OyenteSumar implements ActionListener {
        public void actionPerformed(ActionEvent e)
        {
            //obtengo el numero y le sumo 1
            int numer=Integer.parseInt(numero.getText());
             int rest=numer+1;
             String n=String.valueOf(rest);
            //asigno al Jtextfield el numero
            numero.setText(n);
            manejado m=new manejado();
            m.itemStateChanged(null);
            
         /**Introducir el numero de entradas elegidas al programa**/
            
        }
    }
    
    class manejado implements ItemListener{
       
        @Override
        public void itemStateChanged(ItemEvent ie) {
            String n=numero.getText(); 
            int enteroN= Integer.parseInt(n);
           ClasePelicula.setNumeroEntradas(enteroN);
            System.out.println(ClasePelicula.getNumeroEntradas());
           /** Determina si el check esta marcado o no. Si esta marcado el especial se hara un descuento 
            *La funcion pra hacer el descuento esta en el programa y actua segun del valor de la variable booleana
            * especial que es asignada por la funcion setEspecial
            **/
           if(especial.isSelected()){
               ClasePelicula.setEspecial(true);     
               precio.setText(ClasePelicula.PrecioEntrada());
           }
           
          if(normal.isSelected()){
                  ClasePelicula.setEspecial(false);
                  precio.setText(ClasePelicula.PrecioEntrada());
          } 
            
          /**Asignamos al JTextField el resultado de las operaciones que realiza el metodo del prgrama
           * PrecioTotal. Segun el numero de entradas y si son especiales o normales la cantidad ira variando
           */
           
        }
            
        }
        
           
    //restar
     class OyenteRestar implements ActionListener {
        public void actionPerformed(ActionEvent e)
        {
   
             int numer=Integer.parseInt(numero.getText());
             if(numer<=0){
                 //asi no pasa del 0
             }else{
             int rest=numer-1;
             String n=String.valueOf(rest);
            //asigno al Jtextfield el numero
            numero.setText(n);     
            hacerOperaciones();
             }
        }
        
          public void hacerOperaciones (){
            
            String n=numero.getText(); 
            int enteroN= Integer.parseInt(n);
            ClasePelicula.setNumeroEntradas(enteroN);
            System.out.println(ClasePelicula.getNumeroEntradas());
           
            precio.setText(ClasePelicula.PrecioEntrada());
          
        }
       
 }
    
      class OyenteImprimir implements ActionListener {
        public void actionPerformed(ActionEvent e)
        {
                     // Abrir la ventana de diálogo para seleccionar el archivo para realizar la copia de seguridad
            JFileChooser fileChooser = new JFileChooser(); //cuadro de dialogo
            int returnValue = fileChooser.showSaveDialog(null); //elegir 
            if (returnValue == JFileChooser.APPROVE_OPTION) { 
        //comparamos el valor con una costante que nos dice si ha aprobado la opcion es decir si le ha dado a guardar 
              File selectedFile = fileChooser.getSelectedFile(); 
         //el metodo getSelectedFile de selecion de archivo (fileChooser) permite saber el archivo seleccionado para hacer la copia de seguridad
              
              //contiene la direccion donde voy a guardar el archivo
              String direccion=selectedFile.toString();
              
              System.out.println(direccion);
                
                String titulos=titulo.getText();
                String cines=cine.getText();
               
               ClasePelicula=bd.verInfo(titulos);
               ClaseCine=bd.CargarCines(cines);
               
               String dinero=precio.getText();
               String entrada=numero.getText();
               
              /**
               * Coge el dia del calendario
               */
               ClaseCliente=cl.MostrarUsuarioTemporal();
               String fecha=calendario.getDate().toString();
               
              ClaseCliente.imprimirEntrada(direccion,ClaseCine,ClasePelicula,dinero, entrada,ClaseCliente,fecha);
              
              
              
              System.out.println("Has guardado el informe en el archivo:" +  selectedFile.getName());
            }
            //si le da a cancelar no hace nada 

                    
            
            
             }
        }
 

    
}


    
    
    
    
