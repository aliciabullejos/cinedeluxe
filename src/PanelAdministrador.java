import java.awt.Color;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class PanelAdministrador extends JFrame  {
 

//botones
JButton cine;
JButton pelicula;
JMenuBar op;
JMenuItem salir, cerrar_sesion;

public PanelAdministrador(){
    /*permite iniciar las propiedades de los componentes*/
  iniciarComponentes();
  /**Asigna un titulo a la barra de titulo*/
  setTitle("CineDeluxe-Panel Administrador");
  /**tamaño de la ventana*/
  setSize(480,600);
  /**pone la ventana en el Centro de la pantalla*/
  setLocationRelativeTo(null);
  //ventana estatica
   setResizable(false);
 
}
  
private void iniciarComponentes() {
  
  
  //Establecemos el layout a null para poder elegir nosotros donde colocar los componentes
  setLayout(null);
   getContentPane().setBackground(Color.white); //fondo liso
  
   
   menu();
   
   
  //boton cliente
   this.cine=new JButton();
  this.cine.setText("Cine");
  this.cine.setBounds(140,280,180,25);
  this.cine.addActionListener(new oyente());
  
  
  //boton administrador
  this.pelicula= new JButton();
  this.pelicula.setText("Pelicula");
  this.pelicula.setBounds(140, 380, 180, 25);
  this.pelicula.addActionListener(new oyente2());
  
   //logon empresa
   JLabel logo=new JLabel(); 
   logo.setIcon(new ImageIcon(getClass().getResource("/imagenes/logo300.png"))); 
   getContentPane().add(logo);
   logo.setBounds(140,30, 200, 200);

  /**Agregamos los componentes al Contenedor*/
    add(this.cine);
    add(this.pelicula);

 }

public void menu(){
    
    
    op=new JMenuBar(); 
    // opcion mas pequeña del menu, lista desplegable
     setJMenuBar(op); //asignamos
       //cremos otra objeto 
     JMenu exitMenu = new JMenu("Salir");
     op.add(exitMenu);
     exitMenu.add(cerrar_sesion = new JMenuItem("Cerrar sesion", 'C'));
     exitMenu.add(salir = new JMenuItem("Salir", 'C'));
       //manejadore de cerrar
      salir.addActionListener(new oyente3());
     cerrar_sesion.addActionListener(new oyente4());

}


 
  public class oyente implements ActionListener{

       @Override
       public void actionPerformed(ActionEvent e) {
           
           Administrador_Cine ac=new Administrador_Cine();
           ac.setVisible(true);
           dispose();
           
           
       }
     
 }
  
    public class oyente2 implements ActionListener{

       @Override
       public void actionPerformed(ActionEvent e) {
           
          Administrador_Peliculas ap=new Administrador_Peliculas();
           ap.setVisible(true);//visible
           PanelAdministrador.this.dispose();//ciera la anterior ventana cuando abre esta
           
           
       }
   
 }
    
        public class oyente3 implements ActionListener{

       @Override
       public void actionPerformed(ActionEvent e) {
           
           
            int selector=JOptionPane.showOptionDialog(salir, "¿Quiere salir?", "Salir", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE,null,null,null); 
            
            if(selector==0){

                  System.exit(0);
            }
                 
       }
   
 }
    
    public class oyente4 implements ActionListener{

       @Override
       public void actionPerformed(ActionEvent e) {
           
          login_administrador lg=new login_administrador();
          lg.setVisible(true);
          dispose();
                 
       }
   
 } 
        
        
        
        
        
        
        
 
}
