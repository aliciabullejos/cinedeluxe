

import baseDatos.conexion;
import java.awt.Color;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;


public class login_administrador extends JFrame {
 

JButton entrar;
JLabel labelTitulo, LabelContra;
JTextField usuario;
JPasswordField contra;
conexion bd=new conexion();


 
public login_administrador(){
  /**permite iniciar las propiedades de los componentes*/
  iniciarComponentes();
  /**Asigna un titulo a la barra de titulo*/
  setTitle("CineDeluxe-Login Administrador");
  /**tamaño de la ventana*/
  setSize(480,600);
  /**pone la ventana en el Centro de la pantalla*/
  setLocationRelativeTo(null);
  //ventana estatica
   setResizable(false);
}
 

 
private void iniciarComponentes() {
 
  /**con esto definmos nosotros mismos los tamaños y posicion
   * de los componentes*/
     setLayout(null);
    getContentPane().setBackground(Color.white); //fondo liso
  
  /**Propiedades del boton, lo instanciamos, posicionamos y
   * activamos los eventos*/
  
  
  /**Propiedades del Label, lo instanciamos, posicionamos y
   * activamos los eventos*/
  
  // texto usuario
  this.labelTitulo= new JLabel();
  this.labelTitulo.setText("Usuario");
  this.labelTitulo.setBounds(190, 220, 180, 23);
  this.usuario = new JTextField();
  this.usuario.setText("");
  this.usuario.setBounds(130, 260, 180, 23);
  
  //label contraseña
  this.LabelContra=new JLabel();
  this.LabelContra.setText("Contraseña");
  this.LabelContra.setBounds(180, 300,180,23);
  this.contra = new JPasswordField();
  this.contra.setText("");
  this.contra.setBounds(130, 340, 180, 23);
  
  //boton entrar
  
  entrar= new JButton();
  entrar.setText("Entrar");
  entrar.setBounds(180, 400, 80, 23);
  entrar.addActionListener(new oyenteValidar());
  

//boton volver  
  JButton volver=new JButton();
  volver.setIcon(new ImageIcon(getClass().getResource("/imagenes/volver.png"))); 
   getContentPane().add(volver);
   volver.setBounds(350, 490, 64, 64);
   volver.addActionListener(new oyenteVolver());
   
   
    //logon empresa
   JLabel logo=new JLabel(); 
   logo.setIcon(new ImageIcon(getClass().getResource("/imagenes/logo300.png"))); 
   getContentPane().add(logo);
   logo.setBounds(140,10, 200, 200);
     
  /**Agregamos los componentes al Contenedor*/
   add(labelTitulo);
   add(this.usuario);
   add(this.contra);
   add(this.LabelContra);
   add(entrar);
  
     //fondo login con imagen
   JLabel imagen=new JLabel(); 
   imagen.setIcon(new ImageIcon(getClass().getResource("/imagenes/login.png"))); 
   getContentPane().add(imagen);
   imagen.setBounds(0,90, 700, 500);
   
  
 }

/**
 * Llama al metodo de la clase conexcion en la cual se comprueba si el usuario y la contraseña son correctas
 * y ademas corresponde la una con la otra. 
 */

    public class oyenteValidar implements ActionListener{

       @Override
       public void actionPerformed(ActionEvent e) {
           
           boolean validar; 
           
           String usu=usuario.getText();
           String cont=contra.getText();
           validar=bd.comprobarAdministrador(usu, cont);

            if(validar==true){
                PanelAdministrador pa=new PanelAdministrador();
                 pa.setVisible(true);
                 dispose();
               
         }else{
               JOptionPane.showMessageDialog( null, "Usuario o contraseña incorrecta" );
               
                    usuario.setText(" ");
                    contra.setText("");
               
           }
           
       
       
           
       }
     
 }
 
     public class oyenteVolver implements ActionListener{
         
       public void actionPerformed(ActionEvent e) {
           
           inicio_sesion is= new inicio_sesion();
            is.setVisible(true);
            login_administrador.this.dispose();
         
       }
     
 }

}
