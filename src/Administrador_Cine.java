import Programa.Administrador;
import Programa.Pelicula;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Alicia-Portatil
 */
public class Administrador_Cine extends JFrame {
  JMenuBar op;
  JTabbedPane tabPanel;
  JPanel crear, borrar, mostrar; 
  JTextField nombre, ciudad;
  JTextField nombre1;
  JButton elimino, agrega, muestra; 
  JMenuItem salir, volver;  //componentes del menu
 Administrador ad=new Administrador();
  DefaultTableModel dtm;
 JTable tabla;
  

  public Administrador_Cine(){
      
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE );
        setLayout( new BorderLayout() );
        setSize( 500, 500);
        setTitle( "Administracion Cine" );
        setLocationRelativeTo(null);
         //ventana estatica
        setResizable(false); //ventana fija
       
        
        menu1();  // ACTIVA EL MENU 1
  
        panelCrear();
        panelBorrar();
        panelMostrar();
          
        // Montar las lenguetas en la ventana
        tabPanel = new JTabbedPane();
        /* aqui se crean las pestañas a partir de los paneles */
        tabPanel.addTab( "Añadir nueva Cine" , crear);
        tabPanel.addTab( "Borrar Cine" , borrar );
        tabPanel.addTab( "Mostrar" , mostrar);
        
      
        /**
         * Se pone lo ultimo para arregar todo el tabPanel con sus cosas agragadas al contenedor de la clase
         */
       add( tabPanel, BorderLayout.CENTER);
     
        setVisible(true);

  }
  
  public void menu1()
  {
  
       
    // Barra de menú
    op=new JMenuBar();
     setJMenuBar(op); // añadimos la barra  de menu al panel

    //cremos otra objeto 
    JMenu opcion = new JMenu("Opciones");
    op.add(opcion);
     opcion.add(salir = new JMenuItem("Cerrar", 'C'));
     opcion.add(volver=new JMenuItem("Volver",'V'));
       //manejadore de salir
      salir.addActionListener(new OyenteMenu());
      volver.addActionListener(new OyenteMenu1());
      
      
  }
  
  
  
  public void panelCrear()
  {   
        // PANEL DE LA LENGUETA
      //creamos panel
        crear = new JPanel();
        crear.setLayout(null); //como se van a organizar los componentes 

        // COMPONENTES DEL PANEL- añadimos los componentes   
       
        JLabel Lnombre=new JLabel("Nombre");
       crear.add(Lnombre);
       crear.add(nombre = new JTextField("",15)); //el 15 es el tamaño
       Lnombre.setBounds(50,20,180,25);
       nombre.setBounds(150,20,150, 30);
 
        JLabel Lciudad=new JLabel("Ciudad");
        crear.add(Lciudad);
        crear.add(ciudad= new JTextField("",15));
        Lciudad.setBounds(50,60,180,25);
        ciudad.setBounds(150,60,300, 30);
        
        
              //logon empresa
       JLabel logo=new JLabel(); 
       logo.setIcon(new ImageIcon(getClass().getResource("/imagenes/logoC.png"))); 
       crear.add(logo);
       logo.setBounds(50, 300, 100, 100);
       
        agrega= new JButton(); //crear boton
        agrega.setText("Agregar");//darle  nombre
        agrega.setBounds(250,350, 100, 35);
        crear.add(agrega); //añadir
    
         
         //fondo
       JLabel imagen=new JLabel(); 
        imagen.setIcon(new ImageIcon(getClass().getResource("/imagenes/F2.png"))); 
       crear.add(imagen);
       imagen.setBounds(0, 0, 900, 600);
   
     
        agrega.addActionListener(new OyenteAgregar());

  }

       
    public void panelBorrar()
  {
      //inicializamos componentes
       borrar = new JPanel();
       borrar.setLayout(null); //como se van a organizar los componentes 

       JLabel n=new JLabel("Nombre");
       borrar.add(n);
       borrar.add(nombre1 = new JTextField("",15)); //el 5 es el tamaño
       n.setBounds(50,20,180,25);
       nombre1.setBounds(150,20,150, 30);
  
      elimino= new JButton(); //crear boton
       elimino.setText("Borrar");//darle  nombre
       elimino.setBounds(130,350, 100, 35);
       borrar.add(elimino); //borrar
       elimino.addActionListener(new OyenteBorrar());
       
       muestra=new JButton("Ver cines en Mostrar");
       muestra.setBounds(250,350,200, 35);
       borrar.add(muestra);
       muestra.addActionListener(new OyenteMostrar());
 
            //logon empresa
       JLabel logo=new JLabel(); 
       logo.setIcon(new ImageIcon(getClass().getResource("/imagenes/logoC.png"))); 
       borrar.add(logo);
       logo.setBounds(50, 300, 100, 100);
       
           //fondo
       JLabel imagen=new JLabel(); 
        imagen.setIcon(new ImageIcon(getClass().getResource("/imagenes/F2.png"))); 
       borrar.add(imagen);
       imagen.setBounds(0, 0, 900, 600);
 
  }
 
   public void panelMostrar()
  {
        mostrar = new JPanel();
        mostrar.setPreferredSize( new Dimension(100,50) );
        mostrar.setLayout( new BorderLayout() );
       
     Vector columnas = new Vector();
       
        /* 
            A ese vector le agrego datos, estos datos vendrán a ser las
            columnas de la tabla.
        */
        columnas.add("Nombre");
        columnas.add("Ciudad");
   
        /* 
        La clase AbstractTableModel es la que implementa directamente a la interface
        TableModel, aunque es esta clase la que se recomienda extender para utilizarla como modelo de
        tabla, existe un modelo de tabla predeterminado que facilita mucho el trabajo con tablas. Este
        modelo predeterminado es la clase DefaultTableModel
        */
        
        dtm= new DefaultTableModel(null, columnas);
        
         
        /* 
            Despues de haber creado el modelo de tabla, dtm en el ejemplo , se crea la tabla con el
            constructor correspondiente:
         */
        tabla = new JTable(dtm);
    
        mostrar.add(tabla);     
        
  }

 class OyenteMenu implements ActionListener {
        public void actionPerformed(ActionEvent e) 
        {
            
                     
            int selector=JOptionPane.showOptionDialog(salir, "¿Quiere salir?", "Salir", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE,null,null,null); 
            
            if(selector==0){

                  System.exit(0);
            }
                       
                    
             }

     
 }
 
  class OyenteMenu1 implements ActionListener {
        public void actionPerformed(ActionEvent e) 
        {
            
                    PanelAdministrador p=new PanelAdministrador();
                    p.setVisible(true);
                    dispose();
                       
                    
             }   
 }
  
  class OyenteAgregar implements ActionListener {
        public void actionPerformed(ActionEvent e) 
        {
            
           String n=nombre.getText();
           String c=ciudad.getText(); 
           //Llamo a la consulta que esta en la clase Administrador
           boolean v=ad.insertarCines(n,c);
            
           if(v==true){
             JOptionPane.showMessageDialog( null, "Cine Insertado" ); 
             
                       nombre.setText("");
                       ciudad.setText("");
                       
           }else{
               JOptionPane.showMessageDialog( null, "Error no se puede insertar" );
            
           }
           
        }
                       
                    
             }

  
  class OyenteBorrar implements ActionListener {
        public void actionPerformed(ActionEvent e) 
        {
            
           String n=nombre1.getText();
      
           boolean v=ad.borrarCines(n);
            
           if(v==true){
             JOptionPane.showMessageDialog( null, "Cine Borrado" );  
                         nombre1.setText("");
 
             
           }else{
               JOptionPane.showMessageDialog( null, "Error no se puede borrar" );
            
           }
           
        }
                           
             }     
 
  
  /**
   * A este evento le estamos insertando a un tabla con SetModel el resultado del Metodo MostrarCines que es 
   * una tabla tambien. 
   */
  class OyenteMostrar implements ActionListener {
        public void actionPerformed(ActionEvent e)
        {
            
         tabla.setModel(ad.MostrarCines());    
            System.out.println("");
         
   }
     
  
}          
    

}

    
   
