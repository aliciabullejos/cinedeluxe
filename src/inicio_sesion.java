import baseDatos.conexion;
import java.awt.Color;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;


public class inicio_sesion extends JFrame{

//botones
JButton Cliente;
JButton Administrador;
JButton salir;
conexion bd=new conexion();

public inicio_sesion(){
  //inicializamos los componenetes en un metodo aparte y aqui lo llamamos
  iniciarComponentes();
  //Asigna un titulo a la barra de titulo
  setTitle("CineDeluxe");
  //tamaño de la ventana
  setSize(800,450);
  //para que la pantalla sea estatica
  setResizable(false);
  //pone la ventana en el Centro de la pantalla
  setLocationRelativeTo(null);
    setVisible(true);
  
}
  
private void iniciarComponentes() {
    
    System.out.println("Iniciar componentes");
  
 //Establecemos el layout a null para poder elegir nosotros donde colocar los componentes
  setLayout(null);

  //boton cliente
  this.Cliente=new JButton();
  this.Cliente.setText("Cliente");
  this.Cliente.setBounds(460,250,180,30);
  this.Cliente.addActionListener(new oyente());

  
  //boton administrador
  this.Administrador= new JButton();
  this.Administrador.setText("Administrador");
  this.Administrador.setBounds(140, 250, 180, 30);
  this.Administrador.addActionListener(new oyente2());
  
  /**
   * Añadir una imagen a un boton. Para ellos usamos el metodo SetIcon sobre el boton 
   * e introducimios la imagen con una serie de metodos necesarios para que funcione utilizando los recursos
   * del archivo, luego se lo agregamos al contenedor y le damos tamaños y posicion 
   */
  salir=new JButton();
  salir.setIcon(new ImageIcon(getClass().getResource("/imagenes/salir1.png"))); 
   getContentPane().add(salir);
   salir.setBounds(710, 350, 42, 42);
   this.salir.addActionListener(new oyente3());

 
  /**Agregamos los 3 botones  al Contenedor*/
  add(this.Cliente);
  add(this.Administrador);
  add(this.salir);
  
  //logon empresa
   JLabel logo=new JLabel(); 
   logo.setIcon(new ImageIcon(getClass().getResource("/imagenes/logo300.png"))); 
   add(logo);
   logo.setBounds(300, 10, 200, 200);
   //logo admin
   JLabel logo1=new JLabel(); 
   logo1.setIcon(new ImageIcon(getClass().getResource("/imagenes/admin.png"))); 
   add(logo1); 
   logo1.setBounds(85, 220, 64, 64);
  //logo user
  JLabel logo2=new JLabel(); 
   logo2.setIcon(new ImageIcon(getClass().getResource("/imagenes/users.png"))); 
   add(logo2);
   logo2.setBounds(650,220,64,64);
  /**
   * Para poner añadir una imagen de fondo primero a un JLabel mediante el metodo setIcon le pasamos
   * por parametros una imagen. Los metodos  getClass y getResource son necasarias para la utilizacion 
   * de los recursos de nuestro archivo. Despues lo añadimos al contenedor  con getContentPane().add(la variable del jLabel)
   */ 
   JLabel imagen=new JLabel(); 
   imagen.setIcon(new ImageIcon(getClass().getResource("/imagenes/F1.png"))); 
   add(imagen);
   imagen.setBounds(0, 0, 800, 450);
   
 

 }
 
/**
 * Entra en una ventana nueva para que el cliente pueda logearse y ademas borra la tabla creada temporalmente
 * para poder llenarse con la informacion del logeo nuevo
 */
  public class oyente implements ActionListener{

       @Override
       public void actionPerformed(ActionEvent e) {
           
           login_cliente l=new login_cliente();
           l.setVisible(true);
           inicio_sesion.this.dispose();
           bd.borrarTemporal();
  
       }
     
 }
  
    public class oyente2 implements ActionListener{

       @Override
       public void actionPerformed(ActionEvent e) {
           
           login_administrador l=new login_administrador();
           l.setVisible(true);//visible
           inicio_sesion.this.dispose();//ciera la anterior ventana cuando abre esta
       }
   
 }
    
        public class oyente3 implements ActionListener{

       @Override
       public void actionPerformed(ActionEvent e) {
           
          System.exit(0);
                 
       }
   
 }
    
    
  public static void main(String[] args) {
/**Declaramos el objeto*/
 inicio_sesion miVentanaPrincipal= new inicio_sesion();

  }
}
