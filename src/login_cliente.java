
import Programa.Cliente;
import baseDatos.conexion;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class login_cliente extends JFrame {
 
JButton entrar, registrar; //botones
JLabel labelTitulo, LabelContra,LabelRegistrar, LabelSign; 
JTextField usuario; 
conexion bd=new conexion();
JPasswordField contra;
Cliente  ClaseCliente;

 
public login_cliente(){
 /**permite iniciar las propiedades de los componentes*/
  iniciarComponentes();
  /**Asigna un titulo a la barra de titulo*/
  setTitle("CineDeluxe-Login Cliente");
  /**tamaño de la ventana*/
  setSize(480,600);
  /**pone la ventana en el Centro de la pantalla*/
  setLocationRelativeTo(null);
  //ventana estatica
   setResizable(false);
}

 
private void iniciarComponentes() {
   
  /**con esto definmos nosotros mismos los tamaños y posicion
   * de los componentes*/
  setLayout(null);
  getContentPane().setBackground(Color.white);

  // texto usuario
  this.labelTitulo= new JLabel();
  this.labelTitulo.setText("Usuario");
  this.labelTitulo.setBounds(190, 220, 180, 23);
  this.usuario = new JTextField();
  this.usuario.setText("");
  this.usuario.setBounds(130, 260, 180, 23);
  
  //label contraseña
  this.LabelContra=new JLabel();
  this.LabelContra.setText("Contraseña");
  this.LabelContra.setBounds(180, 300,180,23);
  this.contra = new JPasswordField();
  this.contra.setText("");
  this.contra.setBounds(130, 340, 180, 23);
  
  //boton entrar
  
  entrar= new JButton();
  entrar.setText("Entrar");
  entrar.setBounds(180, 400, 80, 23);
  entrar.addActionListener(new oyenteValidar());
  

//boton volver  
  JButton volver=new JButton();
  volver.setIcon(new ImageIcon(getClass().getResource("/imagenes/volver.png"))); 
   getContentPane().add(volver);
   volver.setBounds(350, 490, 64, 64);
   volver.addActionListener(new oyenteVolver());
   
   //boton registarse 
   this.LabelSign=new JLabel();
   this.LabelSign.setText("SIGN");
   this.LabelSign.setBounds(30, 500,200,50);
   this.LabelRegistrar=new JLabel();
   this.LabelRegistrar.setText("Registrate aqui");
   this.LabelRegistrar.setBounds(30, 470,200,50);
   this.registrar=new JButton();
   registrar.setIcon(new ImageIcon(getClass().getResource("/imagenes/sign.png"))); 
   getContentPane().add(registrar);
   registrar.setBounds(65,510, 30, 30);
   registrar.addActionListener(new oyente2());
   
   
    //logon empresa
   JLabel logo=new JLabel(); 
   logo.setIcon(new ImageIcon(getClass().getResource("/imagenes/logo300.png"))); 
   getContentPane().add(logo);
   logo.setBounds(140,10, 200, 200);
   

  /**Agregamos los componentes al Contenedor*/
            add(labelTitulo);
            add(this.usuario);
            add(this.contra);
            add(this.LabelContra);
            add(entrar);
            add(LabelSign);
            add(LabelRegistrar);
  
    JLabel imagen=new JLabel(); 
   imagen.setIcon(new ImageIcon(getClass().getResource("/imagenes/login.png"))); 
   getContentPane().add(imagen);
   imagen.setBounds(0,90, 700, 500);
   
  
  
  
 }

/**
 * llama al metodo de la clase conexcion en la cual se comprueba si el usuario y la contraseña son correctas
 * y ademas corresponde la una con la otra. Si se cumple habre una ventana nueva y almacena la informacion
 * del usuario o cliente en una tabla temporal para poder acceder a esa informacion aunqye cambie de ventana
 */

    public class oyenteValidar implements ActionListener{

       @Override
       public void actionPerformed(ActionEvent e) {
           boolean v;    
           String usu=usuario.getText();
           String  contrasena=contra.getText();
           v=bd.comprobarUsuario(usu,contrasena);
           
           if(v==true){
               
               UsuarioCine uc=new UsuarioCine();
                 uc.setVisible(true);
                dispose();
                
                ClaseCliente=bd.CargarClientes(usu);
             bd.insertarTemporal(ClaseCliente.getLogin(), ClaseCliente.getPassword(), ClaseCliente.getNombre(), ClaseCliente.getApellido1(),
                  ClaseCliente.getApellido2(), ClaseCliente.mail);
           }
         
           else{
               
                JOptionPane.showMessageDialog(null,"Error, usuario o contraseña incorrecta");
        }
         
       }
     
 }
 
    /**
     * Vuelve ventana anterior
     */
     public class oyenteVolver implements ActionListener{
         
       public void actionPerformed(ActionEvent e) {
           
           inicio_sesion is= new inicio_sesion();
           is.setVisible(true);
           login_cliente.this.dispose();

       }
     
 }
     
     /**
      * Abre ventana registrar
      */
     public class oyente2 implements ActionListener{
         
       public void actionPerformed(ActionEvent e) {
           
           Registrar R=new Registrar();
            R.setVisible(true);
            login_cliente.this.dispose();
         
       }
     
     }
}
