

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Alicia-Portatil
 */
public class InfoPelicula extends JFrame {
  
     JLabel imagen;
      JTextField titulo, director, duracion, precio, horario; 
    JButton volver; 
    JLabel Ltitulo, Ldirector, Lduracion, Lprecio, Lhorario, Lsipnosis; 
    JTextArea sp;
    JScrollPane areaSalida;

  public InfoPelicula(){
    /*Para montar entera el JFrame*/   
    IniciarComponentes();
   /* Titulo de la ventana*/
     setTitle("Registrate");
   /*Tamaño*/   
       setSize(900,600);
   /*inmovible y centrada*/    
    setResizable(false);
    setVisible(true);
    setLocationRelativeTo(null);      
    
    setBackground(Color.white);
   
  }  
    
  public void IniciarComponentes(){
      
      //Montamos el contenedos
  
       setLayout(null);
  
    this.Ltitulo=new JLabel("Titulo: "); 
    Ltitulo.setBounds(50,50 ,180, 33);
    Ltitulo.setFont(new Font("Serif", Font.BOLD, 18)); //ponerle funte y tamaño
    this.titulo=new JTextField("",15);
    titulo.setBounds(200, 50, 200, 30);
    titulo.setEditable(false);
      
  this.Ldirector=new JLabel("Director: "); 
  Ldirector.setBounds(50,100 ,180, 33);
  Ldirector.setFont(new Font("Serif", Font.BOLD, 18)); //ponerle funte y tamaño
  this.director=new JTextField("",15);
   director.setBounds(200, 100, 200, 30);
   director.setEditable(false);
  
   this.Lduracion=new JLabel("Duracion: "); 
  Lduracion.setBounds(50, 150 ,180, 33);
  Lduracion.setFont(new Font("Serif", Font.BOLD, 18)); //ponerle funte y tamaño
  this.duracion=new JTextField("",15);
  duracion.setBounds(200, 150, 120, 30);
  duracion.setEditable(false);
 
   this.Lprecio=new JLabel("Precio: "); 
   Lprecio.setBounds(50,200 ,180, 33);
   Lprecio.setFont(new Font("Serif", Font.BOLD, 20)); //ponerle funte y tamaño
  this.precio=new JTextField("",15);
  precio.setBounds(200, 200, 120, 30);
  precio.setEditable(false);

   this.Lhorario=new JLabel("Horario: "); 
   Lhorario.setBounds(50,250 ,180, 33);
   Lhorario.setFont(new Font("Serif", Font.BOLD, 20)); //ponerle funte y tamaño
  this.horario=new JTextField("",15);  
   horario.setBounds(200, 250, 120, 30);
   horario.setEditable(false);
   
   
   this.Lsipnosis=new JLabel ("Sipnosis");
   Lsipnosis.setBounds(50,300 ,180, 33);
   Lsipnosis.setFont(new Font("Serif", Font.BOLD, 20)); //ponerle funte y tamaño
//  this.sp=new  JTextArea(20,20);
 

         sp= new JTextArea(10,50);
        sp.setLineWrap(true); //para hacer los saltos de linea
        sp.setWrapStyleWord(true);//Para hacer los saltos de lines en los espacios blancoos de la palabra
        JScrollPane scroll = new JScrollPane(sp);
         add(scroll);
        sp.setBounds(140, 300, 300, 200);
   
  this.volver=new JButton("Ver otras"); 
  volver.setBounds(500,450, 100,30 );
  volver.addActionListener(new OyenteVolver());
  
    this. imagen=new JLabel();
     imagen.setBounds(480, 20, 400, 400);
   
  

  add(Lprecio); add(precio);
  add(horario);add(Lhorario);
  add(titulo); add(Ltitulo);
  add(director);add(Ldirector);
  add(duracion); add(Lduracion); 
  add (Lsipnosis); add(sp);
  add(imagen);
  add(volver); 
 

  }

  class OyenteVolver implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent ae) {
           dispose();
           UsuarioCine uc=new UsuarioCine();
            

        }      
  }

  
  
  
}
