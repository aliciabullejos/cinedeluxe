package Programa;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.util.ArrayList;
import java.util.Scanner;

/**
 * Utilizamos una pelicula concreta
 * @author Alicia-Portatil
 */
public class Pelicula  {

//Atributos de pelicula

 public String titulo;
 public String director;
 public String duracion;
 public String precio;
 public String horario;
 public String sipnosis;
 public String imagen;
 public String video;
 public int numeroEntradas;
 public boolean especial=false;
 public String precioTotal;

 /**
  * Constructor por defecto, me sirve para poder crear objetos de Pelicula en otras clases sin tener que 
  * introducir todos los atributos de la clase por paramentros
  */
    public Pelicula() {
    
    }
 
    /**
     * Constructor de la clase
     * @param titulo
     * @param director
     * @param duracion
     * @param precio
     * @param horario
     * @param sipnosis
     * @param imagen
     * @param video 
     */
  public Pelicula(String titulo, String director, String duracion, String precio, String horario, String sipnosis,
          String imagen, String video){
      
        this.titulo = titulo;
        this.director = director;
        this.duracion = duracion;
        this.precio = precio;
        this.horario = horario;
        this.sipnosis = sipnosis;
        this.imagen = imagen;
        this.video = video;
    }      
  
  /**
   * Devuelve un valor
   * @return titulo
   */
     public String getTitulo() {
        return titulo;
    }

     /**
   * Asigna valor al titulo
   * @param titulo
   */
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }
/**
   * Devuelve un valor
   * 
   */
    public String getDirector() {
        return director;
    }
/**
   *Asigna valor al director
   * @param titulo
   */
    public void setDirector(String director) {
        this.director = director;
    }

    /**
   * Devuelve un valor
   * @return duracion
   */
    public String getDuracion() {
        return duracion;
    }

    public void setDuracion(String duracion) {
        this.duracion = duracion;
    }
/**
   * Devuelve un valor
   * @return precio
   */
    public String getPrecio() {
        return precio;
    }
/**
 * Asigna valor al precio
 * @param precio 
 */
    public void setPrecio(String precio) {
        this.precio = precio;
    }

    /**
   * Devuelve un valor
   * @return horario
   */
    public String getHorario() {
        return horario;
    }
/**
 * Asigna valor al horario
 * @param horario 
 */
    public void setHorario(String horario) {
        this.horario = horario;
    }
/**
   * Devuelve un valor
   * @return sipnosis
   */
    public String getSipnosis() {
        return sipnosis;
    }
    /**
     * asigna un valor a sipnosis
     * @param sipnosis 
     */
    public void setSipnosis(String sipnosis) {
        this.sipnosis = sipnosis;
    }
/**
   * Devuelve un valor
   * @return imagen
   */
    public String getImagen() {
        return imagen;
    }
    /**
     * Asigna un valor a imagen
     * @param imagen 
     */
    public void setImagen(String imagen) {
        this.imagen = imagen;
    }
/**
   * Devuelve un valor
   * @return video
   */
    public String getVideo() {
        return video;
    }
/**
 * Asigna valor al video
 * @param video 
 */
    public void setVideo(String video) {
        this.video = video;
    }

    /**
     * Devuelve el numero de entradas 
     * @return 
     */
        public int getNumeroEntradas() {
        return numeroEntradas;
    }
/**
 * Asigna una cantidad al  numero de entradas
 * @param numeroEntradas 
 */
    public void setNumeroEntradas(int numeroEntradas) {
        this.numeroEntradas = numeroEntradas;
    }
 
    /**
     * Asigna true o false a la variable especial
     * @param especial 
     */
    public void setEspecial(boolean especial){
       
    this.especial=especial;
 
    }
    
    /**
     * Calcula el precio total de la entrada, dependiendo de algunas condiciones. 
     * Si la variable especial es true se le aplicara un descuento al precio de la entrada, mientras que si es false
     * el precio de la entrada se mentiene igual. despues se multiplicara por el numero de entradas que haya. 
     * Devuelve un String para poder introducir el valor de este metodo en un JtextField en la interfaz, para operar
     * usamos enteros y depues hacemos un String.valueOf y lo convertimos en cadena
     * @return 
     */
      public String PrecioEntrada(){
 
       int precio2=Integer.parseInt(precio);   
       int precio1; 
       if (especial==true){
     
     precio1=((precio2-2)*numeroEntradas);
           
       }
       
       else{
           
         precio1=precio2*numeroEntradas;
       }
 
        return precioTotal=String.valueOf(precio1);
          
    }
    
    
    
    
    
    
    
    
    
 

    
}