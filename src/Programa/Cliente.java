package Programa;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import javax.swing.text.Document;

/**
 *
 * @author Alicia-Portatil
 */
public class Cliente  {
    
    Connection conexion=null;
  public boolean DatosValidos;  
    public String login;
    public String password; 
    public String nombre;
    public String apellido1;
    public String apellido2;
    public String mail;
    Pelicula p=new Pelicula();
    

    public Cliente (String login, String password, String nombre, String apellido1, String apellido2, String mail) {
        this.login=login;
        this.password=password;
        this.nombre=nombre;
        this.apellido1=apellido1;
        this.apellido2=apellido2;
        this.mail=mail;
    }
 
    /**
     * Aprovecho el constructo por defecto para insertar la conexion a la base de datos y asi poder implementar
     * en esta clase algunas consultas.
     */
  public Cliente(){
      
      try {
              Class.forName("org.gjt.mm.mysql.Driver");
            String usuario="root";
            String password=""; //no tiene contraseña
            String base="cinedeluxe"; //nombre de la base de datos
        
            //incializo la conexion - 3306 es mi puerto de mysql/ local
            conexion=DriverManager.getConnection("jdbc:mysql://localhost:3306/cinedeluxe",usuario,password);
            System.out.println("Se realizo la conexion satisfactoriamente");//para saber si conecta con bd
         
            
         } catch (ClassNotFoundException ex) {
            System.out.println("Error en el driver"+ex.getMessage());
         }catch(SQLException e){ //capturo excepcion de sql 
             System.out.println("Error en la conexion"+e.getStackTrace());
         } 
        

      
  }
  
    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String Nombre) {
        this.nombre = Nombre;
    }

    public String getApellido1() {
        return apellido1;
    }

    public void setApellido1(String Apellido1) {
        this.apellido1 = Apellido1;
    }

    public String getApellido2() {
        return apellido2;
    }

    public void setApellido2(String Apellido2) {
        this.apellido2 = Apellido2;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }
   
    /**
     * Almacenamiento de archivos. creo un fichero y lo relleno con los datos de las clases del programa que 
     * ya han sido almacenados anteriormente
     * @param direccion
     * @param c
     * @param pe
     * @param dinero
     * @param entrada
     * @param cl
     * @param fecha 
     */
    public void imprimirEntrada(String direccion,Cines c,Pelicula pe, String dinero, String entrada,Cliente cl,String fecha){
        
        File file=new File (direccion);
      
           //flujoo
            FileWriter fich =null;
            BufferedWriter b=null;
            PrintWriter pw=null;
            
            //comprobar que el fichero existe, sino crearlo
            if (file.exists()){
                
                System.out.println("El fichero existe");
            }
            else {
                try {
                    System.out.println("Creamos el fichero");
                    file.createNewFile();
                    
                } catch (IOException ex) {
                    ex.getMessage();
                }
            }
            
            try {
           
                //abrimos los flujos
                /*1*/ fich=new FileWriter (file);
                /*2*/ b=new BufferedWriter (fich);
               /*3*/  pw= new PrintWriter (b);
               
               
               pw.print("Nombre:  "); pw.println(cl.getNombre());
               pw.print("Apellidos:  "); pw.print(cl.getApellido1()); pw.println(cl.getApellido2()); 
               pw.print("Titulo: "); pw.println(pe.getTitulo()); 
               pw.print("Cine: "); pw.println(c.getNombre());
               pw.print("Ciudad: "); pw.println(c.getCiudad()); 
               pw.print("Duracion: "); pw.println(pe.getDuracion()); 
               pw.print("Horario:  "); pw.println(pe.getHorario()); 
               pw.print("Precio:  "); pw.println(pe.getPrecio()); 
               pw.print("Numero de entradas: "); pw.println(entrada); 
               pw.print("Precio Total:   "); pw.println(dinero); 
               pw.print("Fecha: "); pw.println(fecha); 
        //cerramos el flujos
            pw.flush();
            pw.close();
            b.close();
            fich.close();
           
            } catch (IOException ex) {
                ex.printStackTrace();
            } 
             
    }
    
    /**
     * Metodo boolean que devuelve true si se cumple las condiciones impuestas para 
 el login (Valor introducido por paramentros)
     * @param login
     * @return DatosValidos
     */
    public boolean validarDatos(String login){
       
       int UsuValido=login.length(); //calcular longitud string
      char caracter=login.charAt(0);//sacar el primer caracter, posicion 0
     
      //Que el login introducido tenga entre 5 y 15 caracteres
       if(UsuValido>=5&& UsuValido<=15){  
           //Que el primer caracter sea una letra
           if(Character.isLetter(caracter)){         
                   DatosValidos=true;
           }      
       else {
           DatosValidos=false;
       }   }
       
   return DatosValidos;
    }
    
  /**
   * Insertta los datos del usuario en la tabla  cliente de la base de datos
   * @param usuario
   * @param password
   * @param nombre
   * @param apellido1
   * @param apellido2
   * @param email 
   */
    public void registrarUsuario(String usuario, String password, String nombre, String apellido1, String apellido2, String email){
  
         try {
           Statement consulta =conexion.createStatement(); //sirve para crear la consulta y haga conexion con la bd
           //ExecuteUpdate solo sirve para insertar datos en una bd con la consulta INSER INTO
            consulta.executeUpdate("INSERT INTO cliente VALUES('"+usuario+"','"+password+"','"+nombre+"','"+apellido1+"','"+apellido2+"','"+email+"')");
            System.out.println("Registrado correctamente:");
          
            
        } catch (SQLException ex) {
            System.out.println("Error en el registro" +ex.getStackTrace());
        }
     }

     /**
         * De esta forma podemos sacar los datos de la tabla temporal creada para guardar al cliente logeado. 
         */   
        public Cliente MostrarUsuarioTemporal(){
            Cliente c=new Cliente();
             
                 try{
            Statement stmt = conexion.createStatement(); 
            String consulta = "SELECT * FROM temporal";
            
                 
                ResultSet resultadoConsulta = stmt.executeQuery(consulta);
                while (resultadoConsulta.next()) {
               
                c.setLogin(resultadoConsulta.getString("usuario"));
                c.setPassword(resultadoConsulta.getString("password"));
                c.setNombre(resultadoConsulta.getString("nombre"));
                c.setApellido1(resultadoConsulta.getString("apellido1"));
                c.setApellido2(resultadoConsulta.getString("apellido2"));
                c.setMail(resultadoConsulta.getString("email"));
                    System.out.println("asdj" + c.getLogin());
                    System.out.println(c.getNombre());
                 
                
             }
        }
        catch(Exception e){
          System.out.println("Error en la consulta" +e.getMessage());
        }
                 return c;
                 
    

    }
    
}
    
    
    
    
    
    
    
    

