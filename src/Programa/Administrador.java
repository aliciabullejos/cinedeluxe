/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Programa;

import java.util.ArrayList;
import baseDatos.conexion;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Alicia-Portatil
 */
public class Administrador {

    Connection conexion=null;
     String login;
     String password; 
     boolean DatosValidos;

    public Administrador( String login, String password) {

        this.login=login;
        this.password=password;
        
    }
    /**
      * Hago una conexion en el constructor por defecto para poder implementar aqui la consulta con la base de datos.
     */
  public Administrador(){
        try {
            
            
            Class.forName("org.gjt.mm.mysql.Driver");
            String usuario="root";
            String password=""; //no tiene contraseña
            String base="cinedeluxe"; //nombre de la base de datos
            
            
            //incializo la conexion - 3306 es mi puerto de mysql/ local
            conexion=DriverManager.getConnection("jdbc:mysql://localhost:3306/cinedeluxe",usuario,password);
            System.out.println("Se realizo la conexion satisfactoriamente");//para saber si conecta con bd
         
            
         } catch (ClassNotFoundException ex) {
            System.out.println("Error en el driver"+ex.getMessage());
         }catch(SQLException e){ //capturo excepcion de sql 
             System.out.println("Error en la conexion"+e.getStackTrace());
         } 
        
    
  }
  
  /**
   * Compruebo que el id tenga menos de 5 caracteres y que empiece por un numero
   * Tambien  compruebo que el precio sea un numero mayor a 0
   * @param id
   * @param precio
   * @return DatosValidos
   */
      public boolean validarDatos(String id, int precio){
       
       int ide=id.length(); //calcular longitud string
      char caracter=id.charAt(0);//sacar el primer caracter, posicion 0
     
      //Que el login introducido tenga entre 5 y 15 caracteres
       if(ide<=5){  
           //Que el primer caracter sea una letra
           if(Character.isDigit(caracter)){         
                   if (precio>0){
                       DatosValidos=true;
                   }}      
       else {
           DatosValidos=false;
       }   }
       
   return DatosValidos;
    }
    
          
          
    /**
     * Inserto los datos de la pelicula en su tabla correspondiente de la base de datos
     * @param id
     * @param titulo
     * @param director
     * @param duracion
     * @param precio
     * @param horario
     * @param sipnosis
     * @param imagen
     * @param video
     * @return 
     */     
 
    public boolean insertarPelicula(String id, String titulo,String director,String duracion,int precio, String horario,String sipnosis, String imagen, String video){
       
        boolean valor=true;
        try {
            Statement consulta=conexion.createStatement(); //sirve para crear la consulta y haga conexion con la bd
            consulta.executeUpdate("INSERT INTO peliculas VALUES('"+id+"','"+titulo+"','"+director+"','"+duracion+"','"+precio+"','"+horario+"','"+sipnosis+"','"+imagen+"','"+video+"')");
           
        } catch (SQLException ex) {
            System.out.println("Error en la inserccion " + ex.getMessage());
            valor=false;
        }
        
        return valor;
    }
    
      /**
       * Sentencia Borrar pelicula
       * @param id de la pelicula
       * @return el valor david explicame que haces nene 
       */
      
       public boolean borrarPelicula(String id){
          boolean valor=true;
          try {
            Statement consulta =conexion.createStatement();
            consulta.executeUpdate("DELETE FROM peliculas WHERE id_pelicula='"+id+"'");
        } catch (SQLException ex) {
            System.out.println("Error en el borrado");
            valor=false;
        }
          return valor;
       
       }    
       
       /**
        * Metodo de tipo tabla y que me devuelve una tabla que luego insertare en la interfaz con los datos 
        * que yo quiero de la pelicula
        * @return 
        */
       
       public  DefaultTableModel MostrarPelicula(){
           
          DefaultTableModel tablamodelo=new DefaultTableModel
        (null,new Object[]{"id_pelicula","titulo","director","duracion","precio", "horario", "sipnosis", "imagen","video"});
          
        //ahora realizo una consulta a base de datos
        //declaramos una consulta a la base de datos
        PreparedStatement consulta;
        try {
            //ahora recojo los datos
            consulta=conexion.prepareStatement("SELECT * FROM peliculas");
            ResultSet rs=consulta.executeQuery();
            while(rs.next()){
                tablamodelo.addRow(new Object[]{ rs.getString("id_pelicula"),rs.getString("titulo"),rs.getString("director"), rs.getString("duracion"),
                    rs.getString("precio"),rs.getString("horario"),rs.getString("sipnosis"),rs.getString("imagen"),rs.getString("video")});
                
            }
        } catch (SQLException ex) {
            System.out.println("Error en la consulta"+ex.getMessage());
        }
        return tablamodelo;
       
       }    
     
       /**
        * Insertar los cines en la bd
        * @param n
        * @param c
        * @return 
        */
      public boolean insertarCines(String n, String c){
        boolean valor=true;
        try {
            Statement consulta1=conexion.createStatement(); //sirve para crear la consulta y haga conexion con la bd
            consulta1.executeUpdate("INSERT INTO cines VALUES('"+n+"','"+c+"')");
           
        } catch (SQLException ex) {
            System.out.println("Error en la inserccion " + ex.getMessage());
            valor=false;
        }
        
        return valor;
    }
      
      /**
       * Permite borrar la fila del cine de la bd cuyo nombre coincida con el introducido
       * @param nombre
       * @return 
       */
           
         public boolean borrarCines(String nombre){
            boolean valor=true;
          try {
            Statement consulta =conexion.createStatement();
            consulta.executeUpdate("DELETE FROM cines WHERE nombre='"+nombre+"'");
        } catch (SQLException ex) {
            System.out.println("Error en el borrado");
            valor=false;
        }
          return valor;
       
       }       
         
         
        public  DefaultTableModel MostrarCines(){
           
          DefaultTableModel tablamodelo=new DefaultTableModel
        (null,new Object[]{"nombre","ciudad"});
          
        //ahora realizo una consulta a base de datos
        //declaramos una consulta a la base de datos
        PreparedStatement consulta;
        try {
            //ahora recojo los datos
            consulta=conexion.prepareStatement("SELECT * FROM cines");
            ResultSet rs=consulta.executeQuery();
            while(rs.next()){
                tablamodelo.addRow(new Object[]{ rs.getString("nombre"),rs.getString("ciudad")});
                
            }
        } catch (SQLException ex) {
            System.out.println("Error en la consulta"+ex.getMessage());
        }
        return tablamodelo;
       
       }    
    
    
    
    
    
    
         
 }
    

